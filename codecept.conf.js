exports.config = {
  output: "./tests/output",
  helpers: {
    Puppeteer: {
      url: "https://yousign.com/en-eu",
      show: false,
      windowSize: "1440x700"
      // waitForNavigation: ["domcontentloaded", "networkidle2"],
      // waitForAction: 500
    },
    Page: {
      require: "./tests/mouse_helper.js"
    }
  },
  include: {},
  mocha: {},
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: "./tests/features/*.feature",
    steps: ["./tests/step_definitions/steps.js"]
  },
  tests: "./tests/tests/*_test.js",
  plugins: {
    screenshotOnFail: {
      enabled: true
    }
  },
  name: "poc-puppeteer"
};
