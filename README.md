# Poc-puppeteer

End 2 End Behavior Driven Development testing process for converting & testing Gherkin written features.

## Usage

Once Gherkin features files are written in **/features** folder, generate step definitions for all **\*.features** files, write tests and run them.

### Setup

`yarn`

### Commands

`yarn test` - run all tests  
`yarn test:features` - only run features tests  
`yarn test:tests` - only run acceptance tests  
`yarn generate` - generate code templates for all undefined steps in the _./feature_ folder  
`yarn debug` - run all tests with complete logs  


## Help

### Puppeteer API

[Config](https://codecept.io/helpers/Puppeteer#configuration)  
[Methods](https://codecept.io/helpers/Puppeteer#methods)

### Deps

[Codecept.js](https://codecept.io/) - E2E Testing framework with BDD emphasis, features/scenarios focus  
[Puppeteer](https://github.com/GoogleChrome/puppeteer) - Headless Chromium using DevTools protocol  

##

### TODO

- Splitting step definitions per feature when generating Gherkin snippets
- Watch mode for `yarn test`
- Switch Puppeteer to [WebDriver](https://codecept.io/webdriver) or [TestCafe](https://codecept.io/testcafe) for cross browser testing later on
