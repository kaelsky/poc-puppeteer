const { I } = inject();

//FAQ
Given("I am on the FAQ page", () => {
  I.amOnPage("https://help.yousign.com/hc/en-gb");
});

When("I search for {string}", string => {
  I.fillField("query", string);
});

When("I press Enter", () => {
  I.pressKey("Enter");
});

Then("{string} should be my first result", string => {
  I.see(string, "li.search-result:first-child");
});

//DND
Given("I am on the DND page", () => {
  I.amOnPage("https://www.w3schools.com/html/html5_draganddrop.asp");
});

When(`I drag image from left box to right box`, () => {
  // I.dragAndDrop("#div1 img", "#div2");
  I.moveCursorTo("img#drag1");
  I.mouseDown();
  I.moveCursorTo("#div2");
  I.mouseUp();
});

Then(`Image should be in right box`, () => {
  I.seeElement("#div2 img");
});
