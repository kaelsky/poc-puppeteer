Feature("Website homepage");

Scenario("Website loads", I => {
  I.amOnPage("https://yousign.com/en-eu");
  I.see("The simpler way of saying yes");
});
