const Helper = codeceptjs.helper;
const puppeteer = require("puppeteer");

class Page extends Helper {
  mouseDown() {
    const { page } = this.helpers.Puppeteer;
    page.mouse.down();
  }

  mouseMove(x, y) {
    const { page } = this.helpers.Puppeteer;
    page.mouse.move(x, y);
  }

  mouseUp() {
    const { page } = this.helpers.Puppeteer;
    page.mouse.up();
  }

  async moveMouseTo(el) {
    const { page } = this.helpers.Puppeteer;
    const element = await page.$(el);
    const bounding_box = await element.boundingBox();

    await this.mouseMove(
      bounding_box.x + bounding_box.width / 2,
      bounding_box.y + bounding_box.height / 2
    );
  }
}

module.exports = Page;
