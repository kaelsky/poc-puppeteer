Feature: FAQ search
  In order to find answers online
  As a user
  I want to be able search the FAQ

  Scenario: Search for how to sign a procedure
    Given I am on the FAQ page
    When I search for "Signing my first signature procedure"
    And I press Enter
    Then "Signing my first signature procedure" should be my first result